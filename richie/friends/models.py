from django.dispatch import receiver
from django.db.models.signals import post_save

from django.contrib.gis.db import models
from django.contrib.gis.db.models.functions import Distance


class Profile(models.Model):
    full_name = models.CharField(max_length=100)
    location = models.CharField(max_length=100)
    point = models.PointField()
    created = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return self.full_name


class ProfileTopNear(models.Model):
    profile = models.ForeignKey(Profile, related_name='top_near')
    profile2 = models.ForeignKey(Profile, related_name='+')
    distance = models.FloatField()


@receiver(post_save, sender=Profile)
def add_top_near(sender, **kwargs):
    instance = kwargs['instance']
    top_near = (Profile.objects
                .exclude(id=instance.id)
                .annotate(dist=Distance('point', instance.point))
                .order_by('dist')[:2])
    
    for top in  top_near:
        obj, created = ProfileTopNear.objects.get_or_create(
            profile=instance, profile2=top, 
            defaults={'distance': top.dist.m})
        
        if obj.distance != top.dist:
            obj.distance = top.dist.m
            obj.save()


class Animal(models.Model):
    name = models.CharField(max_length=100)


class Breed(models.Model):
    animal = models.ForeignKey(Animal)
    name = models.CharField(max_length=100)


class Mascot(models.Model):
    breed = models.ForeignKey(Breed)
    name = models.CharField(max_length=100)
    age = models.IntegerField(blank=True, null=True)
    gender = models.CharField(max_length=10, choices=(
        ('male', 'male'),
        ('female', 'female'),
    ))
    created = models.DateTimeField(auto_now_add=True)