from django.contrib.gis import admin

from .models import Profile, ProfileTopNear



@admin.register(ProfileTopNear)
class ProfileAdmin(admin.ModelAdmin):
    list_display = ('profile', 'profile2', 'distance')


@admin.register(Profile)
class ProfileAdmin(admin.OSMGeoAdmin):
    default_lon = -7033207.31640
    default_lat = -2012351.55454
    default_zoom = 12