from django.conf.urls import url
from richie.home.views import landing
from django.contrib.gis import admin


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', landing)
]
